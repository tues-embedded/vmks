#include <Adafruit_NeoPixel.h>

#define NUM_PIXELS   8
#define LED_PIN     10

Adafruit_NeoPixel led_strip(NUM_PIXELS, LED_PIN, NEO_GRB);

void setup()
{
    DDRD &= ~((1 << DDD4) | (1 << DDD5) | (1 << DDD6));
    PORTD |= ((1 << PORTD4) | (1 << PORTD5) | (1 << PORTD6));
    led_strip.begin();
}

void loop()
{
    uint8_t pixel_pos = (PIND & ((1 << PIND4) | (1 << PIND5) | (1 << PIND6))) >> 4;
    for (uint8_t i = 0; i < NUM_PIXELS; i++)
    {
        led_strip.setPixelColor(i, led_strip.Color(i == pixel_pos ? 255 : 0, 0, 0));
    }
    led_strip.show();
}
